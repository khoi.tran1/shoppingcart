import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { getItemCart } from "../../redux/productSlice";
import {deletecart } from "../../redux/productSlice"
import { getTotalprice } from "../../redux/productSlice";
export function Cart(){
    const cartItems = useSelector(getItemCart);
    const totalPrice = useSelector(getTotalprice);
    const dispatch = useDispatch();
    return (
        <React.Fragment>
        <div className="text-center mt-12 text-3xl font-semibold">GIỎ HÀNG CỦA BẠN</div>
        <div className="w-full h-fit bg-slate-600 flex justify-evenly ">
            <div className="inline text-center">
             {
                 cartItems.map( item => (
                    <div className="w-96 h-52 rounded-lg mt-5 mb-5 flex justify-center text-left bg-white" key={item.id}> 
                    <div className="w-1/2 h-full">
                       <img className="w-40 h-40 rounded-md m-4" src={item.imgTee} alt=""></img>
                    </div>
                    <div className="w-1/2 h-ful relative">
                       <button className="w-2.5 h-2 absolute top-0 right-1" onClick={() => dispatch(deletecart({cartId: item.id}))}>X</button>
                       <h3 className="mt-10">Name:{item.nameTee}</h3>
                       <p>Price:{item.priceTee}</p>
                       <input value={1} type="text" className="w-12 h-6 bg-slate-500"></input>
                    </div>
                  </div>
                 ))
             }
             </div>
             <div className="w-96 h-60 mt-5 mb-5 bg-white rounded-lg">
                 <h3 className="mt-2 text-center text-2xl font-semibold">ĐƠN GIÁ</h3>
                 <p className="mt-1 ml-8 text-3xl text-red-500">Tạm tính:{totalPrice} VND</p>
                 <p className="mt-1 ml-4 ">Phí vận chuyển sẽ được tính ở trang thanh toán.</p>
                 <p className="mt-1 ml-4">Bạn cũng có thể nhập mã giảm giá ở trang thanh toán.</p>
                 <button className="w-80 h-14 rounded-lg ml-8 mt-1 bg-slate-400">Thanh Toán</button>
            </div>
        </div>
        <div className="w-full h-28 bg-black flex justify-evenly">
            <div className="w-32 h-20 bg-white rounded-lg mt-4">
                  <img className="w-full h-full object-contain " src="https://static.dosi-in.com/images/assets/logo/GHN-01.png" alt=""></img>
            </div>
            <div className="w-32 h-20 bg-white rounded-lg mt-4">
            <img className="w-full h-full object-contain " src="https://dosi-in.com/images/icons/jnt.jpg" alt=""></img>
            </div>
            <div className="w-32 h-20 bg-white rounded-lg mt-4">
            <img className="w-full h-full object-contain " src="https://dosi-in.com/images/icons/giaohangtietkiem.jpg" alt=""></img>
            </div>
            <div className="w-32 h-20 bg-white rounded-lg mt-4">
            <img className="w-full h-full object-contain " src="https://static.dosi-in.com/images/page/2021_app-launching/app-ios.png" alt=""></img>
             </div>
             <div className="w-32 h-20 bg-white rounded-lg mt-4">
             <img className="w-full h-full object-contain " src="https://static.dosi-in.com/images/page/2021_app-launching/app-android.png" alt=""></img>
             </div>
             <div className="w-32 h-20 bg-white rounded-lg mt-4">
             <img className="w-full h-full object-contain " src="https://dosi-in.com/images/doins/bo-cong-thuong.png" alt=""></img>
             </div>    
        </div>
        </React.Fragment>
    )
}