import React from "react";
import {Link} from "react-router-dom"
import { FaPhone } from 'react-icons/fa';
import { FaUser } from 'react-icons/fa'
import {FaShoppingBag} from 'react-icons/fa'
import { useSelector } from "react-redux";
import { getCurrent } from "../../redux/productSlice";
export function Navbar(){
    const current = useSelector(getCurrent);
    console.log(current);
    return(
        <div className="flex justify-between w-full h-12 bg-white fixed top-0 left-0 z-20">
            <div className="navleft flex leading-10 font-semibold ">
            <Link to="/"><div>
                <img className="w-28 h-12 " src="https://static.dosi-in.com/images/dosiin-logo.png" alt=""></img>
                </div></Link>
            <Link to="/detail"><div className="w-fit ml-4 mt-3 mr-3 lg:text-xl sm:text-xs  hover:bg-black hover:text-white ">SẢN PHẨM</div></Link>
            <div className="w-fit mr-3 mt-3 lg:text-xl sm:text-xs hover:bg-black hover:text-white">THƯƠNG HIỆU</div>
            <div className="w-fit mr-3 mt-3 lg:text-xl sm:text-xs hover:bg-black hover:text-white">HÀNG MỚI VỀ</div>
            <div className="w-fit mr-3 mt-3 lg:text-xl sm:text-xs hover:bg-black hover:text-white sm:hidden lg:block">BEST SELLER</div>
            <div className="w-fit mr-3 mt-3 lg:text-xl sm:text-xs hover:bg-black hover:text-white sm:hidden lg:block">SỰ KIỆN</div>
            </div>
            <div className="navright h-12">
                <input className="w-64 h-6 m-0 mt-3 mr-2 border-solid border-2" type="text" placeholder="Tìm kiếm..."></input>
            </div>
            <div>
            <button className="w-8 h-7 bg-slate-300 rounded-lg mt-2 mr-1 text-right pl-2 sm:hidden lg:inline">
                <FaPhone/>
                </button>
                <button className="w-8 h-7 bg-slate-300 rounded-lg mt-2 mr-1 pl-2 sm:hidden lg:inline">
                <FaUser/>
                </button>
                <Link to="/cart"><button className="w-8 h-7 bg-slate-300 rounded-lg mt-2 mr-1 pl-2 relative ">
                <FaShoppingBag/>
                <p className="absolute bottom-0 right-1 text-white shadow-lg">{current}</p>
                </button></Link>
            </div>
        </div>
    )
}