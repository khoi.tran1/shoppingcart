import React from "react";
import { dataProduct } from "./data";
import { Product } from "./product";
import { Slider } from "../Slider/slider";
export function Listproduct(productData){
    return( 
        <React.Fragment>
        <Slider/>
        <div className="w-full h-fit bg-slate-200 flex justify-evenly flex-wrap-reverse">
            {
               dataProduct.map(tee => (
                   <Product 
                   key={tee.id}
                   idproduct={tee.id}
                   productData={tee}
                   url={tee.imageUrl}
                    nameTee = {tee.productName}
                    brandTee = {tee.description}
                    priceTee = {tee.productPrice}
                   />
               ))
            }
        </div>
        </React.Fragment>
    )
}
export default Listproduct;