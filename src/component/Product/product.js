import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { addtocart } from "../../redux/productSlice";
export function Product({url,nameTee,brandTee,priceTee,productData,idproduct}) {
    const dispatch = useDispatch(); 
    return(
        <div className="group sm:w-48 sm:h-72 lg:w-64 lg:h-80 rounded-lg mt-5 mb-5 text-left bg-white shadow-lg relative sm:text-xs  lg:text-sm "> 
               <img className="sm:w-18 sm:h-18 lg:w-56 lg:h-56 rounded-md sm:m-0 lg:m-4 mb-0" src={url} alt=""></img>
               <Link to={`/detailproduct/${idproduct}`}><h3 className=" ml-4">Name: {nameTee}</h3></Link>
               <p className="ml-4">Brand: {brandTee}</p>
               <p className="text-red-500 ml-4">Price: {priceTee}</p>
               <button className="absolute left-16 bottom-24 w-28 h-8 bg-slate-200 shadow-lg font-semibold rounded-md  hidden group-hover:block"
               onClick={() => dispatch(addtocart(productData))}>Thêm vào giỏ</button>
        </div>
    )
}
