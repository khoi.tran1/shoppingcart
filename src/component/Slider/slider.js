import React from "react";
import { listSlide } from "../Product/data";
import { FaAngleLeft } from 'react-icons/fa';
import { FaAngleRight } from 'react-icons/fa';
import { useState } from "react";
export function Slider(){
    const [slide, setSlide] = useState(0);
    const [slide1, setSlide1] = useState(1);
    const [slide2, setSlide2] = useState(2);
    const handleNextslide = () => {
       setSlide(slide+1);
       if(slide>=3){setSlide(0)}
       setSlide1(slide1+1);
       if(slide1>=3){setSlide1(0)}
       setSlide2(slide2+1);
       if(slide2>=3){setSlide2(0)}
    }
    const handlePrevSlide = () => {
        setSlide(slide-1);
        if(slide<=0){setSlide(3)}
        setSlide1(slide1-1);
        if(slide1<=0){setSlide1(3)}
        setSlide2(slide2-1);
        if(slide2<=0){setSlide2(3)}
     }
    return(
        <div className="slider group w-full sm:h-32 lg:h-96 flex justify-center mt-14">
           <div className="sm:w-40 sm:h-32
           lg:w-96 lg:h-full bg-orange-600 mr-2 relative">
            <div className="absolute top-0 left-0 w-full h-full bg-[rgba(0,0,0,0.3)]"></div>
            <img src={listSlide[slide]} className="w-full h-full object-cover transition-transform" alt=""></img>
            <button className="w-8 h-10 absolute shadow-md text-2xl pl-1 top-40 right-0 bg-slate-100 z-10 hidden group-hover:block"
            onClick={handlePrevSlide}
            ><FaAngleLeft/></button>   
          </div>  
           <div className="sm:w-96 sm:h-32 lg:w-1/2 lg:h-full bg-blue-200">
            <img src={listSlide[slide1]} className="w-full h-full transition-transform" alt=""></img>
           </div> 
           <div className="sm:w-40 sm:h-32 lg:w-96 lg:h-full bg-red-600 ml-2 relative">
            <div className="absolute top-0 left-0 w-full h-full bg-[rgba(0,0,0,0.3)]"></div>
            <img src={listSlide[slide2]} className="w-full h-full object-cover transition-transform" alt=""></img>
            <button className="w-8 h-10 absolute shadow-md text-2xl pl-1 top-40 left-0 bg-slate-100 z-10 hidden group-hover:block"
            onClick={handleNextslide}
            ><FaAngleRight/></button>   
          </div>   
        </div>
    )
}