import React from 'react';
import ReactDOM from 'react-dom';
import './input.css';
import { Provider } from 'react-redux';
import store from './redux/store';
import App from './App';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { Listproduct } from './component/Product/Listproduct';
import {Cart } from './component/Cart/cart'
import reportWebVitals from './reportWebVitals';
import {Detailproduct} from './component/Product/detailproduct'
ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
  <React.StrictMode>
    <App />
    <Routes>
      <Route path="/" element={<Listproduct/>}></Route>
      <Route path="/cart" element={<Cart/>}></Route>
      <Route path="/detailproduct/:idproduct" element={<Detailproduct/>}></Route>
    </Routes>
  </React.StrictMode>
  </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
