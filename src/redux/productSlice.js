import { createSlice } from "@reduxjs/toolkit";
export const productSlice = createSlice(
    {
        name: 'product',
        initialState:{
            cartItems: [],
            currentItem:0,
        },
        reducers:{
             addtocart:(state,action) =>{
                console.log(action.payload);
                state.cartItems.push({
                    id: action.payload.id,
                    nameTee: action.payload.description,
                    priceTee: action.payload.productPrice,
                    imgTee: action.payload.imageUrl,
                    current: state.currentItem+=1,
                })
             },
            deletecart:(state, action) =>{
               state.cartItems = state.cartItems.filter(
                   cart => cart.id !== action.payload.cartId,
                   state.currentItem-=1,
               )
            },
        },
    },
)
export const getCurrent = state => state.product.currentItem;
export const getItemCart = state => state.product.cartItems;
export const getTotalprice = state => {
    return state.product.cartItems.reduce((total, cart) =>{
        return total += cart.priceTee;
    },0)
}
export const {addtocart, deletecart} = productSlice.actions;
export const productReducer = productSlice.reducer;